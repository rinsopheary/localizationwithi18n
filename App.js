import React, {Component} from 'react';
import {Text, View, TouchableOpacity, StyleSheet, Alert} from 'react-native';
import I18n from './src/locales/i18n';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      changeLanguage: '',
    }
  }
  
  heading = () => {
    return (
      <View style={styles.mainTitle}>
        <Text>Multi Language in React Native</Text>
      </View>
    );
  };
  button = () => {
    return (
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          Alert.alert(
            'Language Selection',
            'Multi language support',
            [
              {
                text: 'Korean',
                onPress: () => {
                  I18n.locale = 'ko';
                  this.setState({changeLanguage: 'korean'});
                },
              },
              {
                text: 'English',
                onPress: () => {
                  I18n.locale = 'en';
                  this.setState({changeLanguage: 'English'});
                },
              },
              {
                text: 'Khmer',
                onPress: () => {
                  I18n.locale = 'kh';
                  this.setState({changeLanguage: 'khmer'});
                },
              },
              {
                text: 'Cancel',
                onPress: () => {
                  console.log('Cancel Pressed');
                },
                style: 'cancel',
              },
            ],
            {cancelable: false},
          );
        }}>
        <Text>Click Change Language</Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.heading()}
        <View style={{flex: 2, width: '90%'}}>
          <Text style={styles.text}>{I18n.t('greeting')}</Text>
          <Text style={styles.text}>{I18n.t('title')}</Text>
          <Text style={styles.text}>{I18n.t('Message')}</Text>
          {this.button()}
        </View>
      </View>
    );
  }
}
export default App;

const styles = StyleSheet.create({
  container: {backgroundColor: '#F5FCFF', flex: 1, alignItems: 'center'},
  text: {
    paddingVertical: 5,
  },
  button: {
    backgroundColor: '#FF5733',
    paddingVertical: 20,
    alignSelf: 'center',
    marginVertical: 50,
    borderRadius: 10,
    paddingHorizontal: 10,
  },
  mainTitle: {
    flex: 1,
    justifyContent: 'center',
  },
});
