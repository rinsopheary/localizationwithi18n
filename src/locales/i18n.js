import I18n, {getLanguages} from 'react-native-i18n';
import kh from './kh';
import en from './en';
import ko from './ko';

I18n.fallbacks = true;
I18n.locale = 'kh';

I18n.translations = {
  kh,
  en,
  ko,
};

getLanguages()
  .then((languages) => {
    // I18nManager.forceRTL(true);
    console.log('getLanguages', languages); // ['en-US', 'en']
  })
  .catch((error) => {
    console.log('getLanguages error : ', error);
  });
export default I18n;
